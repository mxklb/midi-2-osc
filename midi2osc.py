#!/usr/bin/env python
import sys
import rtmidi
import argparse
import logging
import pyliblo3 as liblo
import time
import orjson
import os
import threading, functools
from statistics import median, mean
import gitversion.version as version
from joblib import Parallel, delayed

# Configure argument parser ..
parser = argparse.ArgumentParser(
    description="Start midi to osc publisher..",
    formatter_class=argparse.MetavarTypeHelpFormatter)
parser.add_argument('--midi_channels', type=str, default="1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16", help='The midi channels to use, comma separated')
parser.add_argument('--midi_clock', type=str, default="no", help='Set to yes to capture midi clock events: start, continue, stop & bpm changes')
parser.add_argument('--osc_server', type=str, default="localhost", help='The osc servers url/name to publish to (comma separated)')
parser.add_argument('--osc_port', type=str, default="1377", help='Each osc servers port to be used (comma separated)')
parser.add_argument('--osc_topic', type=str, default="midi", help='Osc base topic to publish to')
parser.add_argument('--osc_map', type=str, default="/app/osc-map.json", help='Midi 2 osc mapping file')
parser.add_argument('--blacklist', type=str, default="", help='Midi devices/ports to be ignored (comma separated)')
parser.add_argument('--cc_threshold', type=float, default=0.0075, help='CC signals faster threshold [s] will be skipped!')
parser.add_argument('--at_threshold', type=float, default=0.0075, help='Aftertouch signals faster threshold [s] will be skipped!')
parser.add_argument('--pitch_threshold', type=float, default=0.0075, help='Pitch wheel signals faster threshold [s] will be skipped!')
parser.add_argument('--pressure_threshold', type=float, default=0.0075, help='Channel pressure signals faster threshold [s] will be skipped!')
args = parser.parse_args()

# Setup logging ..
level = os.getenv('LOGLEVEL', 'INFO')
loglevel = logging.ERROR
if level == 'INFO':
    loglevel = logging.INFO
if level == 'WARNING':
    loglevel = logging.WARNING
if level == 'DEBUG':
    loglevel = logging.DEBUG
if level == 'ERROR':
    loglevel = logging.ERROR
logging.basicConfig(
    format='%(asctime)s.%(msecs)03d %(levelname)-8s %(message)s',
    level=loglevel,
    datefmt='%Y-%m-%d %H:%M:%S')
logger = logging.getLogger()
logger.info("LOG LEVEL " + level)

# Handle real fast midi signals (skip em due to performance issue) ..
now = time.time()
lastMidiCall = [{ 'cc': now, 'aftertouch': now, 'pitch': now, 'pressure': now } for i in range(17)]

# Setup midi channels
midi_channels = args.midi_channels.split(',')

# Activate midi clock capturing
ignoreMidiClock = True
if args.midi_clock == "yes":
    ignoreMidiClock = False

# Check if multiple receivers are configured ..
serverList = args.osc_server.split(',')
portList = args.osc_port.split(',')
if len(serverList) != len(portList):
    logger.error("MISSING OSC Receivers: " + str(serverList) + ' TOPIC = ' + str(portList))
    sys.exit(1)

# Init list of OSC signal receivers ..
oscReceivers = [(receiver, portList[idx]) for idx, receiver in enumerate(serverList)]

# Decorator method to call things in background ..
def runInBackground(f):
    def wrapped(*args, **kwargs):
        threading.Thread(target=functools.partial(f, *args, **kwargs)).start()
    return wrapped

# Send message to all osc receivers parallel ..
@runInBackground
def sendOsc(topic, value, value2=None):
    global oscReceivers
    if value2 == None:
        Parallel(n_jobs=len(oscReceivers))(delayed(liblo.send)(receiver, topic, value) for receiver in oscReceivers)
    else:
        Parallel(n_jobs=len(oscReceivers))(delayed(liblo.send)(receiver, topic, value, value2) for receiver in oscReceivers)

# Initialize osc base topic ..
osc_topic = "/" + args.osc_topic

# Publish version via OSC message
versionString = version.getVersion()
if len(versionString) > 0:
    containerName = "midi-2-osc"
    if ignoreMidiClock == False:
        containerName = "midi-clock"
    sendOsc("/XDMX/version", containerName, versionString)

# Read midi 2 osc message mappings ..
with open(args.osc_map, 'rb') as file:
    data = file.read()
mapping = orjson.loads(data)

logger.info("OSC RECEIVER: " + str(oscReceivers) + ' TOPIC = ' + osc_topic)
logger.info("MIDI-2-OSC MAPPING:")
for key in mapping.keys():
    logger.info(" - " + str(key) + " = " + str(mapping[key]))
logger.info("ACTIVE MIDI CHANNELS: " + str(midi_channels))

# Get midi ports to be ignored ..
midiBlacklist = args.blacklist.split(",")
logger.info("MIDI PORT BLACKLIST: " + str(midiBlacklist))

# Prepare note info
def note_info(midi, decimals=3):
    nr = midi.getNoteNumber()
    hz = round(midi.getMidiNoteInHertz(nr), decimals)
    name = midi.getMidiNoteName(nr)
    return nr, hz, name

# Init midi clock handling
activeBPM = 120
bpmMeasures = 48
latestBPMs = [activeBPM for i in range(bpmMeasures)]
lastClockTime = time.time()
clockCounter = 0
clockPhase = 0
bpmIndex = 0
      
midiPortName = "midi2osc"
if '0' in midi_channels:
    midiPortName += " (clock)"

# Container for midi devices
class MidiDevice():
    def __init__(self, device, port):
        self.port = port
        self.device = device
        self.name = device.getPortName(port)
        self.device.openPort(self.port, midiPortName+" - "+self.name)
        self.device.setCallback(self.send_osc_midi)
        self.device.ignoreTypes(True, ignoreMidiClock, True)

    # Method to send osc messages ..
    def send_osc(self, channel, msg_type, values=[]):
        if len(values) == 0:
            return
        if len(values) == 1:
            sendOsc(osc_topic + '/' + channel + '/' + msg_type, values[0])
        if len(values) == 2:
            sendOsc(osc_topic + '/' + channel + '/' + msg_type, values[0], values[1])

    # Define callback for incoming MIDI events ..
    # midi msg API: https://docs.juce.com/master/classMidiMessage.html
    def send_osc_midi(self, midi):
        global lastClockTime, bpmIndex, activeBPM, clockCounter, latestBPMs, bpmMeasures, lastMidiCall, clockPhase, args
        channel_str = str(midi.getChannel())
        #print(self.name, type(midi), channel_str, str(midi), str(midi_channels))
        #logger.debug("Send OSC from port: " + str(self.name))

        if channel_str not in midi_channels:
            return

        # Remember midi event time
        midiTime = time.time()

        try:
            # Handle MIDI clock events (calculate & publish BPM changes)
            # b'\xfa' = Start, b'\xfb' = Continue, b'\xfc' = Stop, b'\xf8' = Clock
            if channel_str == '0':
                rawMidiMessage = midi.getRawData()
                if rawMidiMessage == b'\xfa':
                    clockCounter = 0
                    logger.info("- MIDI CLOCK START")
                    self.send_osc(channel_str, mapping['START'], [1])
                    return
                elif rawMidiMessage == b'\xfb':
                    logger.info("- MIDI CLOCK CONTINUE")
                    self.send_osc(channel_str, mapping['CONTINUE'], [1])
                    return
                elif rawMidiMessage == b'\xfc':
                    logger.info("- MIDI CLOCK STOP")
                    self.send_osc(channel_str, mapping['STOP'], [1])
                    return
                elif rawMidiMessage != b'\xf8':
                    logger.warning("Unknown midi message on channel 0: " + str(rawMidiMessage))
                    return
                # Calculate time difference
                currentClockTime = time.time()
                timeDelta = currentClockTime - lastClockTime
                lastClockTime = currentClockTime
                # Midi clock sends 24 ticks per quater/beat
                latestBPMs[bpmIndex] = 60 / (timeDelta * 24)
                bpmIndex = (bpmIndex + 1) % len(latestBPMs)
                if clockCounter % 96 == 0:
                    clockPhase = 0
                    logger.debug(f"- MIDI CLOCK BEAT {clockPhase}")
                    self.send_osc(channel_str, mapping['BEAT'], [clockPhase])
                elif clockCounter % 24 == 0:
                    clockPhase = (clockPhase + 1) % 4
                    logger.debug(f"- MIDI CLOCK BEAT {clockPhase}")
                    self.send_osc(channel_str, mapping['BEAT'], [clockPhase*0.25])                    
                # Calculate active BPM from list of latest measured BPMs
                if clockCounter >= bpmMeasures and clockCounter % bpmMeasures == 0:
                    bpm = int( round(mean(latestBPMs)) )
                    if bpm != activeBPM and bpm <= 600:
                        activeBPM = bpm
                        logger.info("- MIDI CLOCK CHANGED -> BPM = " + str(activeBPM))
                        self.send_osc(channel_str, mapping['BPM CHANGED'], [activeBPM])
                clockCounter += 1
                return

            if midi.isNoteOn():
                note, hz, name = note_info(midi)
                velocity = midi.getVelocity()
                #logger.debug("- NOTE ON : {0} [{1}] {2} \t-> {3}".format(note, hz, name, velocity))
                self.send_osc(channel_str, mapping['NOTE ON'], [note, velocity])

            elif midi.isNoteOff():
                note, hz, name = note_info(midi)
                #logger.debug("- NOTE OFF: {0} [{1}] {2}".format(note, hz, name))
                self.send_osc(channel_str, mapping['NOTE OFF'], [note])

            elif midi.isController():
                ccNumber = midi.getControllerNumber()
                ccValue = midi.getControllerValue()
                if midiTime - lastMidiCall[int(channel_str)]['cc'] < args.cc_threshold:
                    #logger.debug("- CONTROLLER {0} -> {1} = SKIPPING FASTER THEN {2}s".format(ccNumber, ccValue, args.cc_threshold))
                    return
                #logger.debug("- CONTROLLER {0} -> {1}".format(ccNumber, ccValue))
                self.send_osc(channel_str, mapping['CONTROLLER'], [ccNumber, ccValue])
                lastMidiCall[int(channel_str)]['cc'] = midiTime

            elif midi.isAftertouch():
                note, hz, name = note_info(midi)
                aftertouch = midi.getAfterTouchValue()
                if midiTime - lastMidiCall[int(channel_str)]['aftertouch'] < args.at_threshold:
                    #logger.debug("- AFTERTOUCH {0} [{1}] {2} \t-> {3} = SKIPPING FASTER THEN {4}s".format(note, hz, name, aftertouch, args.at_threshold))
                    return
                logger.info("- AFTERTOUCH {0} [{1}] {2} \t-> {3}".format(note, hz, name, aftertouch))
                self.send_osc(channel_str, mapping['AFTERTOUCH'], [note, aftertouch])
                lastMidiCall[int(channel_str)]['aftertouch'] = midiTime

            elif midi.isProgramChange():
                value = midi.getProgramChangeNumber()
                #logger.debug("- PROGRAM CHANGE {0}".format(value))
                self.send_osc(channel_str, mapping['PROGRAM CHANGE'], [value])

            elif midi.isPitchWheel():
                value = midi.getPitchWheelValue()
                if midiTime - lastMidiCall[int(channel_str)]['pitch'] < args.pitch_threshold:
                    #logger.debug("- PITCH WHEEL {0} = SKIPPING FASTER THEN {1}s".format(value, args.pitch_threshold))
                    return
                #logger.debug("- PITCH WHEEL {0}".format(value))
                self.send_osc(channel_str, mapping['PITCH WHEEL'], [value])
                lastMidiCall[int(channel_str)]['pitch'] = midiTime
            
            elif midi.isChannelPressure():
                value = midi.getChannelPressureValue()
                if midiTime - lastMidiCall[int(channel_str)]['pressure'] < args.pressure_threshold:
                    #logger.debug("- CHANNEL PRESSURE {0} = SKIPPING FASTER THEN {1}s".format(value, args.pressure_threshold))
                    return
                #logger.debug("- CHANNEL PRESSURE {0}".format(value))
                self.send_osc(channel_str, mapping['CHANNEL PRESSURE'], [value])
                lastMidiCall[int(channel_str)]['pressure'] = midiTime

        except:
            print(str(sys.exc_info()))
            print("MIDI input crashed ..")
            closeAllMidiPorts()
            sys.exit(1)

# Check for newly connected devices ..
def scan4MidiDevices(devices):
    newDevices = {}
    dev = rtmidi.RtMidiIn()
    for i in range(dev.getPortCount()):
        portname = dev.getPortName(i)
        if portname not in devices.keys(): # new device connected
            ignore = False
            for ignorePort in midiBlacklist:
                if ignorePort in portname:
                    ignore = True
                    break
            if ignore == True:
                #logger.debug("SKIPPING PORT: {0}".format(portname))
                continue
            logger.info("OPENING PORT: {0}".format(portname))
            newDevices[portname] = MidiDevice(rtmidi.RtMidiIn(), i)
        elif devices[portname].device.isPortOpen(): # device still connected
            newDevices[portname] = devices[portname]
 #           logger.debug("PORT CONNECTED: {0}".format(portname))
    if len(devices.keys()) > len(newDevices.keys()):
        for portname in devices.keys():
            if portname not in newDevices.keys():
                logger.info("CLOSING PORT: {0}".format(portname))
                devices[portname].device.closePort()
    return newDevices

# Close all midi ports
def closeAllMidiPorts():
    global devices
    for portname in devices.keys():
        if devices[portname].device.isPortOpen():
            logger.info("PORT CLOSED: {0}".format(portname))
            devices[portname].device.closePort()
    devices = {}

# Main loop ..
devices = {}
try:
    while True:
        devices = scan4MidiDevices(devices)
        time.sleep(5)
except:
    print(str(sys.exc_info()))
    closeAllMidiPorts()
    sys.exit(1)
