# MIDI 2 OSC

This receives midi from connected midi devices and publishes midi messages
of pre configured channels to an osc server.

This is successfully tested with a AKAI LPD8 & Arturia BeatStep Pro ..

Hot plug midi devices shall work as long as the host support it.

Note: This is only approved to work on linux!

## Usage & Configuration

    docker-compose up --build

To configure the midi channels to listen to and the OSC receivers, refer
to the *.env* file and to the *midi2osc.py* arguments.

Note that *rtmidi* needs access to the host soundcard to capture midi
events. Check the *docker-compose.yml* for further details.

## Supported MIDI Messages

The following midi messages are supported and get published as osc-messages

- `/BASE/CHANNEL/noteon` key, velocity
- `/BASE/CHANNEL/noteoff` key
- `/BASE/CHANNEL/cc` number, value
- `/BASE/CHANNEL/aftertouch` key, value
- `/BASE/CHANNEL/program` number
- `/BASE/CHANNEL/pitch` value
- `/BASE/CHANNEL/pressure` value

Where `BASE` is the osc base topic string (defaults to 'midi') and
`CHANNEL` is the midi channel number, from which midi was captured.

To handle midi clock events a duplicated midi-clock service is started
by docker-compose. It captures midi clock events on channel 0. It publishes
quarter note clock signals and resulting BPM changes, start, stop, continue.

- `/BASE/CHANNEL/bpm` BPM value
- `/BASE/CHANNEL/start` start, 1
- `/BASE/CHANNEL/stop` stop, 1
- `/BASE/CHANNEL/continue` continue, 1
- `/BASE/CHANNEL/beat` quarter note clock signal, 1

Note: To change midi signal to osc-messages mappings, configure
*osc-map.json* to match your preferred osc setup.
