FROM mxklb/py-audio:2.0.2

WORKDIR /app

COPY gitversion /app/gitversion
COPY osc-map.json /app/osc-map.json

COPY *.py ./
RUN chmod +x midi2osc.py

ENTRYPOINT ["./midi2osc.py"]